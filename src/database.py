import mysql.connector
from datetime import date, datetime


class Sql():
    # definição do banco de dados
    def __init__(self, user, password, host, database):
        self.cnx = mysql.connector.connect(user=user,
                                           password=password,
                                           host=host,
                                           database=database)
        self.create_tables()
        self.criar_usuarios_nativos()


    def executar(self, comando, parametros):
        cs = self.cnx.cursor()
        cs.execute(comando, parametros)
        self.cnx.commit()
        cs.close()
        return True

    def consultar(self, comando, parametros):
        cs = self.cnx.cursor()
        cs.execute(comando, parametros)
        result = []
        for i in cs:
            result.append(i)
        return result

    def __del__(self):
        self.executar("DELETE FROM tb_usuario WHERE usr_usuario='admin' OR usr_usuario='user';", ())
        self.cnx.close()

    def create_tables(self):
        tables = ( # tabela de usuários
        """
        CREATE TABLE IF NOT EXISTS tb_usuario(
            idt_usuario INT PRIMARY KEY AUTO_INCREMENT,
            usr_usuario VARCHAR(20) NOT NULL,
            nme_usuario VARCHAR(50) NOT NULL,
            pwd_usuario VARCHAR(128) NOT NULL,
            alt_usuario INT NOT NULL,
            sxo_usuario CHAR(1)
        );"""
        , 
        # tabela de registros
        """
        CREATE TABLE IF NOT EXISTS tb_registro(
            idt_registro INT PRIMARY KEY AUTO_INCREMENT,
            pso_registro DECIMAL(3,2) NOT NULL,
            dta_registro DATE NOT NULL,
            cod_usuario INT NOT NULL,
            CONSTRAINT fk_usuario_registro FOREIGN KEY (cod_usuario)
                REFERENCES tb_usuario(idt_usuario)
        );""")

        for table in tables:
            self.executar(table, ())

    def criar_usuarios_nativos(self):
        self.executar("""
        INSERT INTO tb_usuario(usr_usuario, nme_usuario, pwd_usuario, alt_usuario, sxo_usuario) VALUES
            ('admin', 'Admin', PASSWORD('admin'), 180,'M'),
            ('user', 'User', PASSWORD('user'), 170, 'F');
        """, ())

    def getIdt(self, user):
        sql = "SELECT idt_usuario FROM tb_usuario WHERE usr_usuario=%s;"
        return self.consultar(sql, [user])

    def getNome(self, idt_pessoa):
        sql = "SELECT nme_usuario FROM tb_usuario WHERE idt_usuario=%s;"
        return self.consultar(sql, [idt_pessoa])

    def getDados(self, idt_usuario):
        sql = "SELECT * FROM tb_registro WHERE cod_usuario=%s ORDER BY dta_registro;"
        return self.consultar(sql, [idt_usuario])
