# Para importar o flask, vá até Configurações > Interpretador do Projeto > "+" adicione uma biblioteca chamada "Flask"
from flask import Flask, render_template, request, session
# Para importar o MySQL, vá até Configurações > Interpretador do Projeto > "+" adicione uma biblioteca chamada "mysql-connector"
from database import Sql

""" Instruções para rodar o Flask :
(abra o terminal no diretório do main, por EXEMPLO: 'C:\\temp\\cua-caude\\src' dentro do src tem o 'main.py')
        set FLASK_APP=main.py
        set FLASK_ENV=development
        set FLASK_DBUG=1
        flask run                   """

app = Flask(__name__)

# LEMBRETE: Configure os parâmetros abaixo de acordo com seu próprio MySQL (usuário, senha, localhost , bancoDeDados)
database = Sql('root', '', 'localhost', 'db_cuacaude')

# A primeira rota é a rota inicial que se abre ao acessar o projeto pelo localhost
@app.route('/')
def index():
    # Explicação; A rota '/' tem como objetivo chamar a função 'index()', que retorna um redirecionamento em html: 'index.html'
    return render_template("index.html")

# A rota  '/login' é acessada atráves de um hyperlink contido na página 'index', que ativa a função 'login()', que retorna o html 'login.html'
@app.route('/login')
def login():
    return render_template("login.html")

# A rota  '/cadastro' é acessada atráves de um hyperlink contido na página 'index', que ativa a função 'cadastro()', que retorna o html 'cadastro.html'
@app.route('/cadastro')
def cadastro():
    return render_template("cadastro.html")

@app.route('/home')
def home(idt):
    u_idt = idt
    nome = database.getNome(u_idt)[0][0]

    return render_template("home.html", nome=nome)

@app.route('/testelogin')
def testelogin():
    return render_template('loginteste.html')

@app.route('/usrpwd', methods=['post'])
def usrpwd():
    usr = request.form['usuario']
    pwd = request.form['pwd']
    comando = "SELECT COUNT(*) as RES FROM tb_usuario " \
              "WHERE usr_usuario=%s AND pwd_usuario=PASSWORD(%s);"

    cs = database.consultar(comando, [usr, pwd])

    resultado = cs[0]

    if resultado == (0,):
        return render_template('usrpwd.html')

    else:
        idt = database.getIdt(usr)
        idt = idt[0][0]

        return home(idt)

@app.route('/pizza', methods=['GET'])
def pizza():
    idt = session['idt']
    dados = database.getDados(idt)
    return render_template('pizza_graph.html', dados=dados)

@app.route('/graph', methods=['GET'])
def graf_pizza(self):
    comando = "SELECT nome_pessoa, COUNT(idt_registro) AS qtd FROM tb_pessoa JOIN tb_registro ON idt_pessoa=cod_pessoa GROUP BY nome_pessoa;"
    cs = self.consultar(comando, ())
    grf = ""
    i = 0
    for [nome, qtd] in cs:
        grf += ", ['" + nome + "', " + str(qtd) + ", '#9999FF']"
        i += 1
    cs.close()
    return render_template('pizza.html', pizza=grf)

