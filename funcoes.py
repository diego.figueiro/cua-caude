#cálculo de IMC

def imc(peso, alt_m, alt_cm):
#o 1º argumento é o peso em Kg, o 2º é a parte em metros da altura e o 3º é a parte em centímetros da altura
    alt_final = alt_m + alt_cm*0.01
    #o argumento é a altura final. parte em metros + parte em cm
    valor = peso/alt_final**2
    return valor


peso, alt_m, alt_cm = 75, 1, 80
#valores fictícios para teste

imc_fic = imc(peso, alt_m, alt_cm)
print(imc_fic)


#teste com usuário
peso = int(input("Digite seu peso (em Kg): "))
alt_m = int(input("Digite a parte em metros de sua altura: "))
alt_cm = int(input("Digite a parte em centímetros de sua altura: "))

imc_usu = imc(peso, alt_m, alt_cm)
print("seu IMC é ", imc_usu)