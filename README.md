<h1>Projeto - Çua Çaúde</h1> 

<p align="center">
   <img src="http://img.shields.io/static/v1?label=STATUS&message=EM%20DESENVOLVIMENTO&color=RED&style=for-the-badge"/>
</p>

> Status do Projeto: Primeiras Sprints Entregue :heavy_check_mark:

...

> Orientador: [Aderbal Botelho](https://br.linkedin.com/in/aderbalbotelho/pt-br)

...

### Tópicos 
:small_blue_diamond: [DOCUMENTAÇÃO](#documentação)

:small_blue_diamond: [Configurando o Git](#configurando-git-bash)

:small_blue_diamond: [Usuários Públicos](#banco-de-dados)


... 
### DOCUMENTAÇÃO
:raising_hand: [1 Participações Individuais](#1-participações-e-contribuições-individuais)

:memo: [2 Descrição do Website](#2-descrição-do-website)

:triangular_ruler: [3 Arquitetura](#3-arquitetura-do-projeto)

:mega: [4 Comentários](#4-comentários-finais)

## 1 Participações e Contribuições Individuais
<p align="justify">

...

Cronograma:

> Realização de reunião no dia 19/09/2020 para idealização do projeto, definição de funcionalidades e estruturação.

> Início do desenvolvimento 20/09/2020.

>Primeiros esboços das features 23/09/2020.

>Período de desenvolvimento do Python, Flask & Data Base - 25/09/2020 á … .

>Período de desenvolvimento do JS, CSS & HTML - 28/09/2020 á … .

>Documentação da parte conceitual definida no GIT - 08/10/2020 á 09/10/2020.

...

Trabalho individual:

- 21907601 - Breno Sanchi Cardoso do Amaral:

Idealização do projeto com ideias pertinentes durante reuniões, código referência em python contendo funções com fórmulas, Idealização e entusiasta da logomarca v1.0 (posteriormente substituída) com base na Psicologia das Cores para Web e Confecção decisiva da Documentação do Projeto.

- 21906410 - Diego Figueiró:

Idealização do projeto, Gestor do Git, estruturação inicial do (READme),  Comentador de código, Instrutor de Git & Flask, DBA - MySQL, Desenvolvedor Python, Debugger de problemas Front-Back-end, Editor da Documentação em Markdown, Apresentação do Projeto.

- 21900953 - Gabriel Vieira Neves:

Idealização do projeto, pontapé inicial da utilização primordial do Framework Flask em python, Desenvolvedor MySQL-Python,  Debugger de problemas Back-end, participação efetiva na documentação. 

- 21804308 - Luis Paulo de Napoli Ramalho:

Idealização do projeto, pontapé inicial no Front-end, estruturas HTML, estilização por CSS, algumas implementações em JS, Criação da logomarca definitiva, Auxílio de Imagem e Comentários na Apresentação do Projeto.

- 21907417 - Shivaram Agnijan Palackpillil:

Idealização do projeto, Definidor da idealização do projeto e nomenclatura, Instrutor de Git, desenvolvimento inicial da implementação de gráficos através da API do Google Charts, Assistente de Debugger e Back-end geral em python.

...

Informações adicionais sobre participações individuais podem ser consultadas verificando os commits realizados no gitlab.




</p>

## 2 Descrição do  Website
<p align="justify">
...

> 2.1 Visão geral:

Destarte, o usuário deve efetuar ou criar um cadastro, para que assim, armazene-se seus dados e histórico em um banco de dados.
Em seguida, há uma mensagem de boas-vindas e uma opção de consultar seu histórico, ou calcular novamente o IMC. Após o cálculo, compara-se com os dados registrados anteriormente através de forma numérica e também graficamente.

- [Logo Marca](src/assets/logocc.png)

- [Tela Inicial](Screenshots/inicial.png)

- [Tela Cadastro](Screenshots/cadastro.png)

- [Tela Login](Screenshots/login.png)

> 2.2 Facilidade de uso:

Implementadas as facilitações,  temos a plataforma disposta de forma extremamente intuitiva. À proporção que nota-se, praticidade e facilidade no uso a tornam uma opção excelente para os usuários. A interação é simples, mas precisa e eficiente, o que não deixa a plataforma rasa. O site exige apenas dados simples de entrada do usuário para manter seu fluxo. 

> 2.3 Lista de features:

O site executa o cálculo do índice de massa corporal a partir de dados fornecidos pelo próprio usuário. Devido ao cadastro obrigatório, fornece-se também um histórico de cálculos feitos anteriormente, para que assim o usuário possa acompanhar sua variação de peso e IMC.
Além disso, ainda há o recurso de acompanhar graficamente as alterações mencionadas, sendo assim, facilitando a compreensão e visualização dos dados
Referência para desenvolvimento de gráficos: https://developers.google.com/chart

> 2.4 Aplicações e benefícios:

Uma vez identificado, o índice de massa corporal pode ser utilizado para classificar o paciente em classes e a partir daí, fazer diagnósticos de alterações não naturais acerca do peso.
É utilizado pela OMS para calcular o índice de obesidade de determinada localidade, mas também pode ser usado para identificar magreza excessiva.

> 2.5 Área/nicho:

A ideia inicial é que o site atenda à pessoas comuns, sem necessariamente ter algum vínculo profissional com a área da medicina, uma vez que é muito simples e fácil de ser utilizado. No entanto, pode sim atender à necessidades de trabalhadores da área.

</p>

## 3 Arquitetura do Projeto
<p align="justify">
...

O aplicação web funciona com o uso da biblioteca FLASK do Python, onde cada endereço digitado na barra de endereços aciona uma rotina na linguagem. Em cada função são feitos os processamentos necessários de regras de negócio, sendo carregada, daí, uma página HTML especificada. 

Além disso, para armazenar e buscar os dados, foi utilizada outra biblioteca chamada mysql-connector, que faz a ponte do Python com o MySQL, executando os códigos na linguagem SQL e retornando os dados necessários.

Ao fazer uso dos dados do usuário e calcular o IMC respectivo de cada data registrada, foi utilizada uma API de gráficos desenvolvida pela Google para JavaScript, de forma que serão exibidos percentuais em gráficos de pizza e evoluções em gráfico de linhas de acordo com as inserções do usuário.

</p>

## 4 Comentários Finais
<p align="justify">
...

O desenvolvimento do projeto foi uma experiência muito interessante, especialmente porque a maioria dos integrantes tinha pouca familiaridade em programação em HTML.Ficamos satisfeitos com o resultado, mas sempre identificamos aspectos que poderiam ter sidos aprimorados.

Achamos que seria interessante ter implementado mais funcionalidades à plataforma, e promover um desenvolvimento um pouco mais aprofundado e criterioso acerca do design.

De toda forma, diante do aspecto limitante, optamos por um resultado mais simplificado visando os prazos necessários para a entrega do projeto, sabendo que mesmo com ideias ambiciosas, devemos ter resultados palpáveis.

</p>


...

## Configurando Git Bash 

<p align="justify">

  0)Se você deseja realmente entender o que está fazendo e deixar tudo configurado redondinho, acompanhe essa playlist com um tutorial bem explicado e seja um bom ultilizador iniciante de Git:
   https://www.youtube.com/watch?v=Jt4Z1vwtXT0&list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_
   
  1)Para trabalhar com o Projeto, primeiro instale o ''Git Bash''

   Faça o download do git em https://git-scm.com/ e instale.

  2)Clique com o botão direito e Abra o Git Bash, em seguida digite o comando:

   git --version

  3)Configure o Git Bash com seu nome de usuário do GitLab e o Email através dos seguintes comandos:

   git config --global user.name “nomedousuariogitlab”

   git config --global user.email “email@sempreceub.com”
   
  4)Verifique suas credenciais se estão corretas:
  
   git config --global --list

  5)Para clonar o projeto, digite:

   git clone "https://gitlab.com/diego.figueiro/cua-caude.git"

  6)Sempre antes de começar qualquer nova alteração ou inserção, use o comando para verificar se está tudo atualizado com o projeto em nuvem:

   git pull

  7)Faça suas inserções e alterações, e ao final verifique o status, adicione e verifique o status novamente:

   git status

   git add .

   git status

  8)Agora adicione uma mensagem curta que defina suas inserções e alterações, usando:

   git commit -m "mensagem aqui"

  9)Estamos quase prontos para enviar suas alterações, mas antes precisamos garantir que não ocorra erro entre versões das suas alterações e dos seus colegas de trabalho, esse passo é extremamente importante, use:

   git pull --rebase

  10)Agora sim, para enviar suas inserções e alterações para o projeto no GitLab, use:

   git push
 
</p>

... 

## Banco de Dados

> :floppy_disk: Usuários Nativos Públicos:

|name|password|token|
| -------- |-------- |-------- |
|admin |admin|true|

|name|password|token|
| -------- |-------- |-------- |
|user |user|true|

... 

## Linguagens, e outras extensões :books:

- [Python](https://www.python.org/doc/)
- [MySQL](https://dev.mysql.com/doc/refman/8.0/en/language-structure.html)
- [HTML](https://devdocs.io/html/)
- [CSS](https://devdocs.io/css/)
- [JavaScript](https://devdocs.io/javascript/)
- [Flask](https://flask.palletsprojects.com/en/1.1.x/)

...

## Tarefas em aberto

Tópicos listados entre tarefas/funcionalidades que ainda precisam ser implementadas no site.

:memo: Aperfeiçoamento do cadastro usuário

:memo: Funcionalidades de gráficos através da API Google Charts

:memo: Publicar o site em um domínio ativo

## Desenvolvedores

Time responsável pelo desenvolvimento do projeto:

- RA: 21907601 [Breno Sanchi](https://gitlab.com/breno.amaral)
- RA: 21906410 [Diego Figueiró](https://gitlab.com/diego.figueiro)
- RA: 21900953 [Gabriel Neves](https://gitlab.com/gabriel.neves)
- RA: 21804308 [Luis Napoli](https://gitlab.com/luis.napoli)
- RA: 21907417 [Shivaram Palackapillil](https://gitlab.com/shivaram.agnijan)

## Licença 

Direitos - [Não compramos licença]() (não copia ai namoral)

Copyright :copyright: 2020 - Çua çaúde
